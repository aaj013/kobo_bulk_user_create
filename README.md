### Steps for bulk user creation in kobotoolbox using django User model
 - Login to kpi docker container using **docker exec -ti <kpi_container_name> bash** and replicate the folder **create_users** inside container 
 - Make a folder with name **create_users**
 - Create init file by `touch __init__.py`
 - Download py script `wget https://gitlab.com/aaj013/kobo_bulk_user_create/-/raw/master/create_user.py`
 - Download csv template `wget https://gitlab.com/aaj013/kobo_bulk_user_create/-/raw/master/usernames.csv`
 - Now run **python manage.py shell**
 - Inside shell, insert **from create_users import create_user** and press enter
 - Next line, insert **create_user.create_user()** and press enter; now the function will work and create users in the list (username from list, password similar to username)